import { ref } from "vue";
import { defineStore } from "pinia";
import { mdiTagPlusOutline } from "@mdi/js";

export const useMessageStore = defineStore("counter", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const showMessage = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };
  const closeMessage = () => {
    message.value = "";
    isShow.value = false;
  };

  return { isShow, message, showMessage, closeMessage, timeout };
});
